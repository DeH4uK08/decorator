package ua.tasks.DeH4uK;

public class JavaDeveloper implements Developer {

    @Override
    public String doWork() {
        return "Write code.";
    }

    @Override
    public String say() {
        return "I am a Java Developer!";
    }

}
