package ua.tasks.DeH4uK;

public interface Developer {

    String doWork();

    String say();

}
