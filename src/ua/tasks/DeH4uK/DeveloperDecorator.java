package ua.tasks.DeH4uK;

public class DeveloperDecorator implements Developer {

    private Developer developer;

    DeveloperDecorator(Developer developer) {
        this.developer = developer;
    }

    @Override
    public String doWork() {
        return developer.doWork();
    }

    @Override
    public String say() {
        return developer.say();
    }

}
