package ua.tasks.DeH4uK;

public class SeniorJavaDeveloper extends DeveloperDecorator {

    SeniorJavaDeveloper(Developer developer) {
        super(developer);
    }

    private String sendWeeklyReport() {
        return " Send weekly report to the customer.";
    }

    @Override
    public String doWork() {
        return super.doWork() + sendWeeklyReport();
    }

    @Override
    public String say() {
        return super.say() + " I am a Senior Java Developer!";
    }

}
