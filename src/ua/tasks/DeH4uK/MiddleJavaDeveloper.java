package ua.tasks.DeH4uK;

public class MiddleJavaDeveloper extends DeveloperDecorator {

    MiddleJavaDeveloper(Developer developer) {
        super(developer);
    }

    private String makeCodeReview() {
        return " Make code review.";
    }

    @Override
    public String doWork() {
        return super.doWork() + makeCodeReview();
    }

    @Override
    public String say() {
        return super.say() + " I am a Middle Java Developer!";
    }

}
