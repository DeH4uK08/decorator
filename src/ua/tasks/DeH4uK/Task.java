package ua.tasks.DeH4uK;

public class Task {

    public static void main(String[] args) {
        Developer developer = new SeniorJavaDeveloper(new MiddleJavaDeveloper(new JavaDeveloper()));
        System.out.println(developer.doWork());
        System.out.println(developer.say());
    }

}
